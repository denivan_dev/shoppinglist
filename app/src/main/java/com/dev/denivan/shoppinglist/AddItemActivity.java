package com.dev.denivan.shoppinglist;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddItemActivity extends Activity{
    private final String MAIN_LOGGER = "MAIN_LOGGER";
    private final int ADD_STRING_ITEM_REQUEST_CODE = 1;
    private final int ADD_CAMERA_ITEM_REQUEST_CODE = 2;
    private final int ADD_GALLERY_ITEM_REQUEST_CODE = 3;

    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_item);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.cancel, R.id.text_item, R.id.camera, R.id.gallery_item})
    public void onClick(View view) {
        Intent intent;

        switch (view.getId()){
            case R.id.text_item:
                intent = new Intent(this, AddTextItemActivity.class);
                startActivityForResult(intent, ADD_STRING_ITEM_REQUEST_CODE);
                break;
            case R.id.camera:
                if(Build.VERSION.SDK_INT > 22){
                    String[] permissions = {"android.permission.CAMERA"};
                    requestPermissions (permissions, 1);
                } else
                    dispatchTakePictureIntent();
                break;
            case R.id.gallery_item:
                if(Build.VERSION.SDK_INT > 22){
                    String[] permissions = {"android.permission.WRITE_EXTERNAL_STORAGE"};
                    requestPermissions (permissions, 2);
                } else
                    dispatchGalleryIntent();
                break;
            case R.id.cancel:
                Log.d(MAIN_LOGGER, String.valueOf(view.getId()));
                break;
        }
    }

    public void dispatchGalleryIntent(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(
                intent,
                ADD_GALLERY_ITEM_REQUEST_CODE);
    }

    private File createImageFile() throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        );

        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d(MAIN_LOGGER, "createImageFile image.getAbsolutePath(): " + image.getAbsolutePath());
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                Log.d(MAIN_LOGGER, "Failed to create file:");
                Log.d(MAIN_LOGGER, ex.getMessage());
                ex.printStackTrace();
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.example.android.fileprovider",
                        photoFile);

                Log.d(MAIN_LOGGER, "dispatchTakePictureIntent photoURI getPath: " + photoURI.getPath());

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, ADD_CAMERA_ITEM_REQUEST_CODE);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(MAIN_LOGGER, "onActivityResult");
        String new_item;

        switch (requestCode){
            case ADD_STRING_ITEM_REQUEST_CODE:
                new_item = data.getStringExtra("new_item");
                break;
            case ADD_CAMERA_ITEM_REQUEST_CODE:
                new_item = mCurrentPhotoPath;
                break;
            case ADD_GALLERY_ITEM_REQUEST_CODE:
                if (data != null) {
                    Uri selectedImage = data.getData();

                    //
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null,
                            null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String imageFilePath = cursor.getString(columnIndex);
                    cursor.close();
                    new_item = imageFilePath;
                    //

                    Log.d(MAIN_LOGGER, "imageFilePath: " + imageFilePath);
                } else
                    return;
                break;
            default:
                return;
        }

        Intent intent = new Intent();
        Bundle extras = new Bundle();

        extras.putString("EXTRA_NEW_ITEM_VALUE", new_item);
        extras.putInt("EXTRA_NEW_ITEM_TYPE", requestCode);
        intent.putExtras(extras);

        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if(grantResults[0] == PackageManager.PERMISSION_GRANTED)
            switch (requestCode){
                case 1:
                    dispatchTakePictureIntent();
                    break;
                case 2:
                    dispatchGalleryIntent();
            }
        else
            return;
    }
}
