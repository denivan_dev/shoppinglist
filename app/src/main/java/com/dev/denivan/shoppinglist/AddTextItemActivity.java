package com.dev.denivan.shoppinglist;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddTextItemActivity extends AppCompatActivity {
    private final String MAIN_LOGGER = "MAIN_LOGGER";
    @BindView(R.id.item_description)
    EditText et;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_text_item);

        ButterKnife.bind(this);
    }

    @OnClick({R.id.add_text_item_btn})
    public void onClick(){
        String description = et.getText().toString();
        Log.d(MAIN_LOGGER, "AddTextItemActivity: " + description);

        Intent intent = new Intent();

        intent.putExtra("new_item", description);
        setResult(RESULT_OK, intent);
        finish();

    }

}
