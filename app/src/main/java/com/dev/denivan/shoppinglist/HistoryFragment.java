package com.dev.denivan.shoppinglist;

import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dev.denivan.shoppinglist.persisted.History;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HistoryFragment extends Fragment{

    @BindView(R.id.empty_history_message)
    TextView tv;

    @BindView(R.id.history_grid)
    GridLayout gl;

    private final String MAIN_LOGGER = "MAIN_LOGGER";
    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_two, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)this.getActivity()).getPresenter().refreshHistory((MainActivity)this.getActivity());

        return view;
    }

    public void refresh(List<History> items){

        gl.removeAllViews();
        tv.setVisibility(View.INVISIBLE);

        for(History item : items){
            gl.addView(createItem(item.getType(), item.getDescriptionOrPath()));

            Log.d(MAIN_LOGGER, "HistoryFragment item - type: " + item.getType());
            Log.d(MAIN_LOGGER, "HistoryFragment item - descriptionOrPath: " + item.getDescriptionOrPath());
        }
        gl.setAlignmentMode(GridLayout.VERTICAL);
    }

    public RelativeLayout createItem(int type, String content){
        LayoutInflater inflater = getLayoutInflater();
        RelativeLayout rl;

        if(type != 1){
            rl = (RelativeLayout)inflater.inflate(R.layout.image_grid_item, gl, false);

            ImageView iv = rl.findViewById(R.id.grid_item_image);
            iv.setImageURI(Uri.parse(content));
        } else {
            rl = (RelativeLayout)inflater.inflate(R.layout.text_grid_item, gl, false);

            TextView tv = rl.findViewById(R.id.description);
            tv.setText(content);
        }

        CheckBox chB = rl.findViewById(R.id.grid_item_checkbox);
        chB.setVisibility(View.INVISIBLE);

        return rl;
    }
}