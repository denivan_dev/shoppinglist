package com.dev.denivan.shoppinglist;

import com.dev.denivan.shoppinglist.persisted.AppDatabase;
import com.dev.denivan.shoppinglist.persisted.History;

import java.util.List;

import javax.inject.Inject;

public class HistoryModel {
    @Inject
    public HistoryModel(){

    }
    public List<History> getAll(AppDatabase db){
        return db.getHistoryDao().getAll();
    }

}
