package com.dev.denivan.shoppinglist;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.GridLayout;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.dev.denivan.shoppinglist.persisted.ShoppingList;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class ListFragment extends Fragment implements CompoundButton.OnCheckedChangeListener {
    @BindView(R.id.empty_list_message)
    TextView tv;

    @BindView(R.id.list_grid)
    GridLayout gv;

    private final String MAIN_LOGGER = "MAIN_LOGGER";
    private final int ADD_ITEM_REQUEST_CODE = 1;
    Map<Integer, Integer> shoppingListItems = new HashMap<>();
    Map<Integer, Integer> chboxToShLstItemMap = new HashMap<>();
    LinkedList<Integer> checkedItems = new LinkedList<>();
    private List<Integer> checkedItemsIds;

    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.fragment_one, container, false);
        ButterKnife.bind(this, view);

        ((MainActivity)this.getActivity()).getPresenter().refreshList((MainActivity)this.getActivity());

        return view;
    }

    @OnClick({R.id.floatingActionButton})
    public void showAddItemDialog(View v){
        Intent intent = new Intent(this.getActivity(), AddItemActivity.class);
        startActivityForResult(intent, ADD_ITEM_REQUEST_CODE);
        Log.d(MAIN_LOGGER, "showAddItemDialog Main");
    }

    public void refresh(List<ShoppingList> items){
        checkedItemsIds = new LinkedList<>();

        gv.removeAllViews();
        shoppingListItems = new HashMap<>();;
        checkedItems = new LinkedList<>();

        tv.setVisibility(View.INVISIBLE);

        for(ShoppingList item : items){
            gv.addView(createItem(item));


            Log.d(MAIN_LOGGER, "ListFragment item - type: " + item.type);
            Log.d(MAIN_LOGGER, "ListFragment item - descriptionOrPath: " + item.descriptionOrPath);
        }
        gv.setAlignmentMode(GridLayout.VERTICAL);
    }

    public Integer[] getCheckedItemsIds(){
        Integer[] res = checkedItemsIds.toArray(new Integer[checkedItemsIds.size()]);
        return res;
    }

    public RelativeLayout createItem(ShoppingList item){
        LayoutInflater inflater = getLayoutInflater();
        RelativeLayout rl;

        if(item.type != 1){
            rl = (RelativeLayout)inflater.inflate(R.layout.image_grid_item, gv, false);

            ImageView iv = rl.findViewById(R.id.grid_item_image);
            iv.setImageURI(Uri.parse(item.descriptionOrPath));
        } else {
            rl = (RelativeLayout)inflater.inflate(R.layout.text_grid_item, gv, false);

            TextView tv = rl.findViewById(R.id.description);
            tv.setText(item.descriptionOrPath);
        }

        CheckBox chB = rl.findViewById(R.id.grid_item_checkbox);

        int newchBId = View.generateViewId();
        int newRlId = View.generateViewId();

        chB.setId(newchBId);
        rl.setId(newRlId);

        shoppingListItems.put(newchBId, newRlId);
        chboxToShLstItemMap.put(newchBId, item.getId());
        chB.setOnCheckedChangeListener(this);

        return rl;
    }

    @Override
    @OnCheckedChanged(R.id.check_all)
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        if(compoundButton.getId() == R.id.check_all){
            CheckBox cb;
            for(Integer shoppingListItem : shoppingListItems.keySet()){
                cb = gv.findViewById(shoppingListItem);
                cb.setChecked(b);
                Log.d(MAIN_LOGGER,"shoppingListItem: " + shoppingListItem);

                //onCheckedChanged(cb, b);
            }
            Log.d(MAIN_LOGGER,"returnreturnreturn");
            return;
        }

        Log.d(MAIN_LOGGER,"compoundButton.getId(): " + compoundButton.getId());
        if(b && !checkedItems.contains(compoundButton.getId()))
            checkedItems.add(compoundButton.getId());
        else if(checkedItems.contains(compoundButton.getId()))
            checkedItems.remove((Integer) compoundButton.getId());
    }

    @OnClick(R.id.move_to_history)
    public void onMoveToHistoryClicked(){
        for (Integer i : checkedItems){
            Log.d(MAIN_LOGGER,"shoppingListItems.get(" + i + "): " + shoppingListItems.get(i));

            checkedItemsIds.add(chboxToShLstItemMap.get(i));
            gv.removeView(gv.findViewById(shoppingListItems.get(i)));
        }
        ((MainActivity)this.getActivity()).getPresenter().notifyMovedToHistory((MainActivity)this.getActivity());
    }
}