package com.dev.denivan.shoppinglist;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.dev.denivan.shoppinglist.persisted.History;
import com.dev.denivan.shoppinglist.persisted.ShoppingList;

import java.util.ArrayList;
import java.util.List;

import dagger.Component;

public class MainActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Bundle newItem;
    Presenter presenter;
    private static Context context;
    ViewPagerAdapter adapter;

    @Component
    interface PresenterComponent {
        Presenter getPresenter();
    }

    private final String MAIN_LOGGER = "MAIN_LOGGER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MainActivity.context = getApplicationContext();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        presenter = DaggerMainActivity_PresenterComponent.create()
                .getPresenter();
    }

    public static Context getAppContext() {
        return MainActivity.context;
    }

    private void setupViewPager(ViewPager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ListFragment(), "Список");
        adapter.addFragment(new HistoryFragment(), "История");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data == null) {return;}

        newItem = data.getExtras();
        presenter.notifyNewItemAdded(this);
    }

    public void refreshList(List<ShoppingList> items){
        if(items.toArray().length == 0) return;
        for(ShoppingList s : items){
            Log.d(MAIN_LOGGER, "refreshList s.getDescriptionOrPath(): " + s.getDescriptionOrPath());
            Log.d(MAIN_LOGGER, "refreshList s.type: " + s.type);
        }
        ListFragment lFragment = (ListFragment)adapter.getItem(0);
        lFragment.refresh(items);
    }

    public void refreshHistory(List<History> items){
        if(items.toArray().length == 0) return;
        HistoryFragment hFragment = (HistoryFragment)adapter.getItem(1);
        hFragment.refresh(items);
    }

    public Presenter getPresenter() {
        return presenter;
    }

    public Integer[] getAddedItemsIds(){
        ListFragment lFragment = (ListFragment)adapter.getItem(0);
        return lFragment.getCheckedItemsIds();
    }

    public Bundle getNewItem() {
        return newItem;
    }
}
;