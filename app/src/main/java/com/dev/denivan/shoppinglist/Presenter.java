package com.dev.denivan.shoppinglist;

import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.util.Log;
import com.dev.denivan.shoppinglist.persisted.AppDatabase;
import com.dev.denivan.shoppinglist.persisted.History;
import com.dev.denivan.shoppinglist.persisted.ShoppingList;
import java.util.List;
import javax.inject.Inject;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class Presenter {
    private final String MAIN_LOGGER = "MAIN_LOGGER";
    ShoppingListModel slModel;
    HistoryModel hModel;
    AppDatabase db;

    @Inject
    public Presenter(ShoppingListModel slModel, HistoryModel hModel) {
        this.slModel = slModel;
        this.hModel = hModel;

        db = Room.databaseBuilder(MainActivity.getAppContext(),
                AppDatabase.class, "sl-database").build();
    }


    public void refreshList(MainActivity activity){
        Observable.fromCallable(() -> slModel.getAll(db)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((items) -> {
                    activity.refreshList(items);
                });
    }

    public void refreshHistory(MainActivity activity){
        Observable.fromCallable(() -> hModel.getAll(db)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((items) -> {
                    activity.refreshHistory(items);
                });
    }


    public void insertAndRefreshView(MainActivity activity, Bundle newItem){
        Observable.fromCallable(() -> slModel.insertAndGetAll(db, newItem)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((items) -> {
                    activity.refreshList(items);
                });
    }

    public void notifyNewItemAdded(MainActivity activity) {
        Bundle newItem = activity.getNewItem();

        String value = newItem.getString("EXTRA_NEW_ITEM_VALUE");
        int type = newItem.getInt("EXTRA_NEW_ITEM_TYPE");

        Log.d(MAIN_LOGGER, "Presenter - value:" + value);
        Log.d(MAIN_LOGGER, "Presenter - type:" + type);

        insertAndRefreshView(activity, newItem);
    }

    public void notifyMovedToHistory(MainActivity activity){
        Integer[] items = activity.getAddedItemsIds();

        Observable.fromCallable(() -> slModel.moveToHistoryAndGetAll(db, items)).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((listAndHistoryItems) -> {
                    activity.refreshList((List<ShoppingList>)listAndHistoryItems.get(0));
                    activity.refreshHistory((List<History>)listAndHistoryItems.get(1));
                });
    }
}
