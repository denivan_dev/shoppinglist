package com.dev.denivan.shoppinglist;

import android.os.Bundle;

import com.dev.denivan.shoppinglist.persisted.AppDatabase;
import com.dev.denivan.shoppinglist.persisted.History;
import com.dev.denivan.shoppinglist.persisted.ShoppingList;

import java.util.LinkedList;
import java.util.List;
import javax.inject.Inject;

public class ShoppingListModel {
    @Inject
    public ShoppingListModel(){

    }

    public List<ShoppingList> getAll(AppDatabase db){
        return db.getShoppingListDao().getAll();
    }

    public List<ShoppingList> insertAndGetAll(AppDatabase db, Bundle newItem){
        addNewItem(db, newItem);
        return db.getShoppingListDao().getAll();
    }

    public List<List<?>> moveToHistoryAndGetAll(AppDatabase db, Integer[] items){
        List<List<?>> listAndHistoryItems = new LinkedList<>();

        List<ShoppingList> sl = db.getShoppingListDao().selectByIds(items);
        History h = new History();
        for(ShoppingList item : sl){
            h.setType(item.getType());
            h.setDescriptionOrPath(item.getDescriptionOrPath());

            db.getHistoryDao().insert(h);
        }

        db.getShoppingListDao().deleteByIds(items);

        listAndHistoryItems.add(db.getShoppingListDao().getAll());
        listAndHistoryItems.add(db.getHistoryDao().getAll());

        return listAndHistoryItems;
    }

    public void addNewItem(AppDatabase db, Bundle newItem){
        ShoppingList sl = new ShoppingList();

        sl.type = newItem.getInt("EXTRA_NEW_ITEM_TYPE");
        sl.descriptionOrPath = newItem.getString("EXTRA_NEW_ITEM_VALUE");

        db.getShoppingListDao().insert(sl);
    }
}
