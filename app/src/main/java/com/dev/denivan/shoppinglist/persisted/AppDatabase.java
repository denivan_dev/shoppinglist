package com.dev.denivan.shoppinglist.persisted;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

@Database(entities = {ShoppingList.class, History.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract ShoppingListDao getShoppingListDao();
    public abstract HistoryDao getHistoryDao();
}