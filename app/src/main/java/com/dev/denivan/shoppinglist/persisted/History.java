package com.dev.denivan.shoppinglist.persisted;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity
public class History {
    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "type")
    private Integer type;

    @ColumnInfo(name = "description_or_path")
    private String descriptionOrPath;

    public Integer getId(){
        return id;
    }

    public void setId(Integer id){
        this.id = id;
    }

    public Integer getType(){
        return type;
    }

    public void setType(Integer type){
        this.type = type;
    }

    public String getDescriptionOrPath(){
        return descriptionOrPath;
    }

    public void setDescriptionOrPath(String descriptionOrPath){
        this.descriptionOrPath = descriptionOrPath;
    }
}
