package com.dev.denivan.shoppinglist.persisted;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface HistoryDao {
    @Query("SELECT * FROM History")
    List<History> getAll();

    @Insert
    void insert(History item);
}