package com.dev.denivan.shoppinglist.persisted;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface ShoppingListDao {

    @Query("SELECT * FROM ShoppingList")
    List<ShoppingList> getAll();

    // error: INSERT query type is not supported yet. You can use:SELECT, DELETE, UPDATE
    /*@Query("INSERT INTO History SELECT * FROM ShoppingList WHERE id IN (:ids)")
    void moveToHistory(int[] ids);*/

    @Query("DELETE FROM ShoppingList WHERE id IN (:ids)")
    void deleteByIds(Integer[] ids);

    @Query("SELECT * FROM ShoppingList WHERE id IN (:ids)")
    List<ShoppingList> selectByIds(Integer[] ids);

    @Insert
    void insert(ShoppingList item);

    /*default Maybe<List<ShoppingList>> insertAndGetAll(ShoppingList item){
        insert(item);
        return getAll();
    }*/
}
